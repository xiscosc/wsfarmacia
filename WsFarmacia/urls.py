from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework import routers
from farmacia.views import *

router = routers.DefaultRouter()
router.register(r'categorias', CategoriaViewSet)
router.register(r'medicamentos', MedicamentoViewSet)
router.register(r'entradas', EntradaViewSet)
router.register(r'farmacias', FarmaciaViewSet)
router.register(r'noticias', NoticiaViewSet)
router.register(r'salidas', SalidaViewSet)


urlpatterns = [
    # Examples:
    # url(r'^$', 'WsFarmacia.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^admin/', include(admin.site.urls)),
]
