# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Categorias',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('nombre', models.CharField(max_length=60)),
                ('texto', models.CharField(max_length=512)),
                ('imagen', models.CharField(max_length=128)),
            ],
            options={
                'db_table': 'categorias',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Entradas',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('cantidad', models.IntegerField()),
                ('fechahora', models.DateTimeField()),
            ],
            options={
                'db_table': 'entradas',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Farmacias',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('nik', models.CharField(max_length=64)),
                ('pass_field', models.CharField(max_length=64, db_column='pass')),
                ('nivel', models.IntegerField()),
            ],
            options={
                'db_table': 'farmacias',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Medicamentos',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('texto', models.CharField(max_length=512)),
                ('imagen', models.CharField(max_length=128)),
                ('nombre', models.CharField(max_length=64)),
                ('codigo', models.CharField(max_length=64)),
                ('enalmacen', models.IntegerField()),
            ],
            options={
                'db_table': 'medicamentos',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Noticias',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('texto', models.CharField(max_length=512)),
                ('inicio', models.DateTimeField()),
                ('fin', models.DateTimeField()),
            ],
            options={
                'db_table': 'noticias',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Salidas',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('fechahora', models.DateTimeField()),
                ('cantidad', models.IntegerField()),
            ],
            options={
                'managed': False,
            },
            bases=(models.Model,),
        ),
    ]
