# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('farmacia', '0003_auto_20141206_1741'),
    ]

    operations = [
        migrations.AlterField(
            model_name='categoria',
            name='imagen',
            field=models.ImageField(upload_to=b''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='medicamento',
            name='imagen',
            field=models.ImageField(upload_to=b''),
            preserve_default=True,
        ),
    ]
