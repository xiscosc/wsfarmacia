# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('farmacia', '0004_auto_20141215_1715'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entrada',
            name='fechahora',
            field=models.DateTimeField(auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='salida',
            name='fechahora',
            field=models.DateTimeField(auto_now_add=True),
            preserve_default=True,
        ),
    ]
