# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('farmacia', '0005_auto_20150112_1556'),
    ]

    operations = [
        migrations.AlterField(
            model_name='noticia',
            name='fin',
            field=models.DateField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='noticia',
            name='inicio',
            field=models.DateField(),
            preserve_default=True,
        ),
    ]
