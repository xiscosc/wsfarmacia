# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('farmacia', '0002_auto_20141206_1549'),
    ]

    operations = [
        migrations.RenameField(
            model_name='entrada',
            old_name='idmedicamento',
            new_name='medicamento',
        ),
        migrations.RenameField(
            model_name='medicamento',
            old_name='idcategoria',
            new_name='categoria',
        ),
        migrations.RenameField(
            model_name='salida',
            old_name='idfarmacia',
            new_name='farmacia',
        ),
        migrations.RenameField(
            model_name='salida',
            old_name='idmedicamento',
            new_name='medicamento',
        ),
    ]
