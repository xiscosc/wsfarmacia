# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('farmacia', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Categoria',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=60)),
                ('texto', models.CharField(max_length=512)),
                ('imagen', models.CharField(max_length=128)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Entrada',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cantidad', models.IntegerField()),
                ('fechahora', models.DateTimeField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Farmacia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nik', models.CharField(max_length=64)),
                ('pass_field', models.CharField(max_length=64)),
                ('nivel', models.IntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Medicamento',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('texto', models.CharField(max_length=512)),
                ('imagen', models.CharField(max_length=128)),
                ('nombre', models.CharField(max_length=64)),
                ('codigo', models.CharField(max_length=64)),
                ('enalmacen', models.IntegerField()),
                ('idcategoria', models.ForeignKey(to='farmacia.Categoria')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Noticia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('texto', models.CharField(max_length=512)),
                ('inicio', models.DateTimeField()),
                ('fin', models.DateTimeField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Salida',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fechahora', models.DateTimeField()),
                ('cantidad', models.IntegerField()),
                ('idfarmacia', models.ForeignKey(to='farmacia.Farmacia')),
                ('idmedicamento', models.ForeignKey(to='farmacia.Medicamento')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.DeleteModel(
            name='Categorias',
        ),
        migrations.DeleteModel(
            name='Entradas',
        ),
        migrations.DeleteModel(
            name='Farmacias',
        ),
        migrations.DeleteModel(
            name='Medicamentos',
        ),
        migrations.DeleteModel(
            name='Noticias',
        ),
        migrations.DeleteModel(
            name='Salidas',
        ),
        migrations.AddField(
            model_name='entrada',
            name='idmedicamento',
            field=models.ForeignKey(to='farmacia.Medicamento'),
            preserve_default=True,
        ),
    ]
