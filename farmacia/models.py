from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible

import datetime

@python_2_unicode_compatible
class Categoria(models.Model):

    nombre = models.CharField(max_length=60)
    texto = models.CharField(max_length=512)
    imagen = models.ImageField()

    def __str__(self):
        return self.nombre

@python_2_unicode_compatible
class Medicamento(models.Model):
    categoria = models.ForeignKey(Categoria)
    texto = models.CharField(max_length=512)
    imagen = models.ImageField()
    nombre = models.CharField(max_length=64)
    codigo = models.CharField(max_length=64)
    enalmacen = models.IntegerField()

    def __str__(self):
        return self.nombre


@python_2_unicode_compatible
class Entrada(models.Model):
    medicamento = models.ForeignKey(Medicamento)
    cantidad = models.IntegerField()
    fechahora = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return self.fechahora


@python_2_unicode_compatible
class Farmacia(models.Model):
    nik = models.CharField(max_length=64)
    pass_field = models.CharField(max_length=64)  # Field renamed because it was a Python reserved word.
    nivel = models.IntegerField()

    def __str__(self):
        return self.nik

@python_2_unicode_compatible
class Noticia(models.Model):
    texto = models.CharField(max_length=512)
    inicio = models.DateField()
    fin = models.DateField()

    def __str__(self):
        return self.texto

    def is_valid(self):
        actual_date = datetime.datetime.now().date()
        if self.inicio <= actual_date and self.fin >= actual_date:
            return 1
        else:
            return 0

@python_2_unicode_compatible
class Salida(models.Model):
    medicamento = models.ForeignKey(Medicamento)
    fechahora = models.DateTimeField(auto_now_add=True, blank=True)
    cantidad = models.IntegerField()
    farmacia = models.ForeignKey(Farmacia)


    def __str__(self):
        return self.fechahora