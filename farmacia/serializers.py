__author__ = 'xiscosastre'
from rest_framework import serializers
from models import *


class CategoriaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Categoria
        fields = ('url', 'nombre', 'texto', 'imagen', 'id')


class MedicamentoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Medicamento
        fields = ('url', 'nombre', 'texto', 'imagen', 'codigo', 'enalmacen', 'categoria', 'id')


class EntradaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Entrada
        fields = ('url', 'medicamento', 'cantidad', 'fechahora', 'id')


class FarmaciaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Farmacia
        fields = ('url', 'nik', 'pass_field', 'nivel', 'id')


class NoticiaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Noticia
        fields = ('url', 'texto', 'inicio', 'fin', 'id', 'is_valid')


class SalidaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Salida
        fields = ('url', 'medicamento', 'fechahora', 'cantidad', 'farmacia', 'id')


